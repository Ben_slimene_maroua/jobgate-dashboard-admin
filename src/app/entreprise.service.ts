import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EntrepriseService {

  constructor(private http: HttpClient) { }

  token=localStorage.getItem('token')!;
  headersoption= new HttpHeaders({
    Authorization: 'Bearer ' + this.token,
  });
  getallentreprises(){
    return this.http.get(`${environment.baseUrl}/admin/getentreprise`);
  }
  getEntreprise(id:any){
return this.http.get(`${environment.baseUrl}/admin/getUserById/${id}`);
  }
  confirmeduser(id: any, etat:any){
    return this.http.put(`${environment.baseUrl}/admin/confirmuser/${id}`,etat, {headers: this.headersoption})
  }
}
