import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { PlacesService } from '../places.service';

@Component({
  selector: 'app-addplaces',
  templateUrl: './addplaces.component.html',
  styleUrls: ['./addplaces.component.css']
})
export class AddplacesComponent implements OnInit {
  addForm: FormGroup;
  submitted = false;
  listplaces:any;
    constructor(private  placeservice: PlacesService,private formbuilder: FormBuilder) { }
  
    ngOnInit(): void {
      this.addForm=this.formbuilder.group({
       adresse:['',Validators.required],
      })
      this.getallplaces();
    }
    get f() { return this.addForm.controls; }
  
    onSubmit() {
        this.submitted = true;
        this.placeservice
        .createPlace(this.addForm.value).subscribe((res:any)=>{
            console.log("response",res)
            Swal.fire("Place ajouté")
          
        })
        
    }
  
    onReset() {
        this.submitted = false;
        this.addForm.reset();
    }
  
    getallplaces() {
      this.placeservice.getplace().subscribe((res: any) => {
        this.listplaces = res['data'];
      });
    }
  }
