import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { SpecialitesService } from '../specialites.service';

@Component({
  selector: 'app-specialite',
  templateUrl: './specialite.component.html',
  styleUrls: ['./specialite.component.css']
})
export class SpecialiteComponent implements OnInit {

  listsp: any;
  updateForm: FormGroup;
  closeResult = '';
  submitted = false;
  search_nom: any;
  p:number=1;
  constructor(
    private spservice: SpecialitesService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal
  ) {}
  ngOnInit(): void {
    this.getallsp();
    this.updateForm = this.formBuilder.group({
      _id: ['', Validators.required],
     nom: ['', Validators.required],
     description: ['', Validators.required],
     
    });

  }
  getallsp() {
    this.spservice.getSpecialite().subscribe((res: any) => {
      this.listsp = res['data'];
    });
  }

  deletspecialite(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085D6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.spservice.deletSpecialite(id).subscribe((res: any) => {
          Swal.fire( 'Votre specialité est supprimé.', 'success');
          this.getallsp();
        });
      }
    });
  }
  open(content: any, i: any) {
    this.updateForm.patchValue({
      _id: i._id,
      nom: i.nom,
      description: i.description,
    });
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  updatesp() {
    this.spservice
      .updateSpecialite(this.updateForm.value._id, this.updateForm.value)
      .subscribe((res: any) => {
        console.log(res);
        Swal.fire('Specialité modifiée');
        this.getallsp();
      });
  }
}