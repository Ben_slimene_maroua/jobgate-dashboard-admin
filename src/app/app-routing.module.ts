import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddplacesComponent } from './addplaces/addplaces.component';
import { AddspecialitesComponent } from './addspecialites/addspecialites.component';
import { CandidatComponent } from './candidat/candidat.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { DetailEntrepriseComponent } from './detail-entreprise/detail-entreprise.component';
import { DetailcandidatComponent } from './detailcandidat/detailcandidat.component';
import { DetailoffreComponent } from './detailoffre/detailoffre.component';
import { EntrepriseComponent } from './entreprise/entreprise.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { AuthGuard } from './guards/auth.guard';
import { HomeComponent } from './home/home.component';
import { LayoutComponent } from './layout/layout.component';
import { LoginComponent } from './login/login.component';
import { OffreComponent } from './offre/offre.component';
import { PlacesComponent } from './places/places.component';
import { ProfilComponent } from './profil/profil.component';
import { RegistreComponent } from './registre/registre.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { SpecialiteComponent } from './specialite/specialite.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'registre', component: RegistreComponent },
  {path:'forgetPassword',component:ForgetPasswordComponent},
  {path:'resetpassword',component:ResetPasswordComponent},

  {
    path: 'home',canActivate:[AuthGuard],
    component: HomeComponent,
    children: [
      { path: '', component: LayoutComponent },
      { path: 'entreprise', component: EntrepriseComponent },
      { path: 'detailentreprise/:id', component:DetailEntrepriseComponent },
      {path:'changepwd',component:ChangePasswordComponent},
      {path:'candidat',component:CandidatComponent},
      {path:'detailcandidat/:id',component:DetailcandidatComponent},
      {path:'places',component:PlacesComponent},
      {path:'addplaces',component:AddplacesComponent},
      {path:'addsp',component:AddspecialitesComponent},
      {path:'sp',component:SpecialiteComponent},
      {path:'profil',component:ProfilComponent},
      {path:'offre',component:OffreComponent},
      {path:'detailoffre/:id',component:DetailoffreComponent}
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
