import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { PlacesService } from '../places.service';

@Component({
  selector: 'app-places',
  templateUrl: './places.component.html',
  styleUrls: ['./places.component.css']
})
export class PlacesComponent implements OnInit {
  listplaces: any;
  updateForm: FormGroup;
  closeResult = '';
  submitted = false;
  search_nom: any;
  p:number=1;
  constructor(
    private placeservice: PlacesService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal
  ) {}
  ngOnInit(): void {
    this.getallplaces();
    this.updateForm = this.formBuilder.group({
      _id: ['', Validators.required],
     adresse: ['', Validators.required],
     
    });

  }
  getallplaces() {
    this.placeservice.getplace().subscribe((res: any) => {
      this.listplaces = res['data'];
    });
  }

  deleteplaces(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085D6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.placeservice.deleteplace(id).subscribe((res: any) => {
          Swal.fire('Deleted!', 'Your Place has been deleted.', 'success');
          this.getallplaces();
        });
      }
    });
  }
  open(content: any, i: any) {
    this.updateForm.patchValue({
      _id: i._id,
      adresse: i.adresse,
    });
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  updateplaces() {
    this.placeservice
      .updateplace(this.updateForm.value._id, this.updateForm.value)
      .subscribe((res: any) => {
        console.log(res);
        Swal.fire('Place Updated');
        this.getallplaces();
      });
  }
}