import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { ForgetPasswordService } from '../forget-password.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
resetForm: FormGroup
  constructor(private forgetservice:ForgetPasswordService, private formbuilder: FormBuilder) { }

  ngOnInit(): void {
    this.resetForm=this.formbuilder.group({
      resetPasswordToken:['',Validators.required],
      newpassword:['',Validators.required],
    })
  }
reset(){
  return this.forgetservice.resetPassword(this.resetForm.value).subscribe((res:any)=>{
    Swal.fire('OK.', 'success');
console.log(this.resetForm.value)
  })
}
}
