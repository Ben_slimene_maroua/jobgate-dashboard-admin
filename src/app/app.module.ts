import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HomeComponent } from './home/home.component';
import { LayoutComponent } from './layout/layout.component';
import { EntrepriseComponent } from './entreprise/entreprise.component';
import { HttpClientModule } from '@angular/common/http';
import { RegistreComponent } from './registre/registre.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { LoginComponent } from './login/login.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { DetailEntrepriseComponent } from './detail-entreprise/detail-entreprise.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { CandidatComponent } from './candidat/candidat.component';
import { DetailcandidatComponent } from './detailcandidat/detailcandidat.component';
import { SpecialiteComponent } from './specialite/specialite.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { PlacesComponent } from './places/places.component';
import { AddplacesComponent } from './addplaces/addplaces.component';
import { RecherchePipe } from './pipes/recherche.pipe';
import { AddspecialitesComponent } from './addspecialites/addspecialites.component';
import { Recherche1Pipe } from './pipes/recherche1.pipe';
import { ProfilComponent } from './profil/profil.component';
import { OffreComponent } from './offre/offre.component';
import { DetailoffreComponent } from './detailoffre/detailoffre.component';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    HomeComponent,
    LayoutComponent,
    EntrepriseComponent,
    RegistreComponent,
    LoginComponent,
    ForgetPasswordComponent,
    DetailEntrepriseComponent,
    ChangePasswordComponent,
    DetailEntrepriseComponent,
    CandidatComponent,
    DetailcandidatComponent,
    SpecialiteComponent,
    ResetPasswordComponent,
    PlacesComponent,
    AddplacesComponent,
    RecherchePipe,
    AddspecialitesComponent,
    Recherche1Pipe,
    ProfilComponent,
    OffreComponent,
    DetailoffreComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule,
    NgxPaginationModule,

   
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
