import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PlacesService {

  constructor(private http: HttpClient) { }
  token=localStorage.getItem('token')!;
  headersoption= new HttpHeaders({
    Authorization: 'Bearer ' + this.token,
  });
  createPlace(place:any){
    return this.http.post(`${environment.baseUrl}/places/createplaces`,place, {headers: this.headersoption})
  }

  getplace(){
    return this.http.get(`${environment.baseUrl}/places/getplaces`, {headers: this.headersoption});
  }; 
  deleteplace(id:any){
    return this.http.delete(`${environment.baseUrl}/places/deleteplaces/${id}`,{headers: this.headersoption});
  }
  updateplace(id:any,place:any){
    return this.http.put(`${environment.baseUrl}/places/upplaces/${id}`,place,{headers: this.headersoption});
  }
}
