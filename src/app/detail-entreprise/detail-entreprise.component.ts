import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EntrepriseService } from '../entreprise.service';

@Component({
  selector: 'app-detail-entreprise',
  templateUrl: './detail-entreprise.component.html',
  styleUrls: ['./detail-entreprise.component.css']
})
export class DetailEntrepriseComponent implements OnInit {
  listentreprise:any;
  id=this.activeroute.snapshot.params['id'];

  constructor(private entrepriseService: EntrepriseService, private activeroute: ActivatedRoute) { }

  ngOnInit(): void {
    this.getEntreprise();
  }
getEntreprise(){
  return this.entrepriseService.getEntreprise(this.id).subscribe((res:any)=>{
    this.listentreprise=res["data"];
    console.log('liste entreprise :', this.listentreprise);
  });
  
}

}
