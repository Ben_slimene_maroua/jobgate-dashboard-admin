import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { ForgetPasswordService } from '../forget-password.service';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {
ForgetForm: FormGroup;
email:any;
  constructor(private forgetservice: ForgetPasswordService, private formbuilder: FormBuilder, private route: Router) { }

  ngOnInit(): void {
    this.ForgetForm=this.formbuilder.group({
      email:['',Validators.required],
    })
  
  }
  Forgetpassword(){
    return this.forgetservice.forgetPassword(this.ForgetForm.value).subscribe((res:any)=>{
      Swal.fire('Un lien est envoyé par mail veuillez verifier votre email.', 'success');
     console.log("ok")
    })
    this.route.navigateByUrl('/resetpassword')
  }

}
