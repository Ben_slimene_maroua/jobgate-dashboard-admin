import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {

  userconnect= JSON.parse(localStorage.getItem("userconnect")!)
  constructor(private formbuilder:FormBuilder, private route: Router) { }

  ngOnInit(): void {
    console.log(this.userconnect)
  }

}
