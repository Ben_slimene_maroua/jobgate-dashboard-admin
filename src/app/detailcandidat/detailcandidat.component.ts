import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { CandidatService } from '../candidat.service';

@Component({
  selector: 'app-detailcandidat',
  templateUrl: './detailcandidat.component.html',
  styleUrls: ['./detailcandidat.component.css']
})
export class DetailcandidatComponent implements OnInit {
  listcandidat:any;
  allcandidats:any;
  id=this.activeroute.snapshot.params['id'];

  constructor(private candidatService: CandidatService, private activeroute: ActivatedRoute) { }

  ngOnInit(): void {
    this.getEntreprise();
    this.getallcandidat();
  }
getEntreprise(){
  return this.candidatService.getCandidat(this.id).subscribe((res:any)=>{
    this.listcandidat=res["data"];
    console.log('liste candidat :', this.listcandidat);
  });
  
}
getallcandidat(){
  return this.candidatService.getallcandidats().subscribe((res:any)=>{
    this.allcandidats=res["data"].filter(
      (element: any) => element.role == 'candidat'
    );
    console.log('liste candidat:', this.allcandidats);
  })
}

confirmuser(id: any, etat: any) {
  this.candidatService.confirmeduser(id, etat).subscribe((res: any) => {
    console.log(res);
    this.getEntreprise();
  });
}
deleteuser(id: any) {
  Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!',
  }).then((result) => {
    if (result.isConfirmed) {
      this.candidatService.deleteUser(id).subscribe((res: any) => {
        Swal.fire('Deleted!', 'Your file has been deleted.', 'success');
        this.getallcandidat();
      });
    }
  });
}
}