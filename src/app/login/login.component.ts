import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { LoginService } from '../login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  LogForm: FormGroup;
  user:any;
  constructor(private loginservice: LoginService, private formbuilder: FormBuilder, private route: Router) { }

  ngOnInit(): void {
    this.LogForm=this.formbuilder.group({
      email:['',Validators.required],
      password:['',Validators.required]
    })
  }
  login(){
    return this.loginservice.Login(this.LogForm.value).subscribe((res:any)=>{
      console.log(res)
  this.user=res["restok"]
    if (res.message==='login validé avec succée') {
      Swal.fire({
        icon:'success',
        title:'user found',
        text:'email valid',
        footer:'password valid'
        
      })
      this.route.navigateByUrl('/home/entreprise')
      localStorage.setItem('userconnect',JSON.stringify(res.user))
      localStorage.setItem('token',res.token)
      localStorage.setItem("state","0")
      
    }

   } ,err=>{
      Swal.fire({
        icon:'error',
        title:'user not found',
        text:'email invalid',
        footer:'password invalid'
      })
    }
    )
  }
}
