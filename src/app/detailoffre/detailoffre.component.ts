import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { OffreService } from '../offre.service';

@Component({
  selector: 'app-detailoffre',
  templateUrl: './detailoffre.component.html',
  styleUrls: ['./detailoffre.component.css']
})
export class DetailoffreComponent implements OnInit {
listoffre:any;
listof: any;
p:number=1;

  id=this.activeroute.snapshot.params['id'];

  constructor(private offreService: OffreService, private activeroute: ActivatedRoute) { }

  ngOnInit(): void {
    this.getOffre();
    this.getalloff();
  }
getOffre(){
  return this.offreService.getoffre(this.id).subscribe((res:any)=>{
    this.listoffre=res["data"];
    console.log('liste offre :', this.listoffre);
  });
}

getalloff() {
 return  this.offreService.getallOffre().subscribe((res: any) => {
    this.listof = res['data'];
  });
}
deleteoffre(id:any){
  Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085D6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!',
  }).then((result) => {
    if (result.isConfirmed) {
   this.offreService.deleteOffre(id).subscribe((res:any)=>{
    Swal.fire( 'Offre supprimée.', 'success');
    this.getalloff();
  });
}
});
}

}
