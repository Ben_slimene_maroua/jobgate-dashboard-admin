import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddspecialitesComponent } from './addspecialites.component';

describe('AddspecialitesComponent', () => {
  let component: AddspecialitesComponent;
  let fixture: ComponentFixture<AddspecialitesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddspecialitesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddspecialitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
