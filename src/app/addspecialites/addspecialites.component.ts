import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { SpecialitesService } from '../specialites.service';

@Component({
  selector: 'app-addspecialites',
  templateUrl: './addspecialites.component.html',
  styleUrls: ['./addspecialites.component.css']
})
export class AddspecialitesComponent implements OnInit {

  addForm: FormGroup;
  submitted = false;
  listsp:any;
    constructor(private spservice:SpecialitesService,private formbuilder: FormBuilder) { }
  
    ngOnInit(): void {
      this.addForm=this.formbuilder.group({
       nom:['',Validators.required],
       description:['',Validators.required],
      })
      this.getallSp();
    }
    get f() { return this.addForm.controls; }
  
    onSubmit() {
        this.submitted = true;
        this.spservice.createSpecialite(this.addForm.value).subscribe((res:any)=>{
            console.log("response",res)
            Swal.fire("Specialité ajouté")
          
        })
        
    }
  
    onReset() {
        this.submitted = false;
        this.addForm.reset();
    }
  
    getallSp() {
      this.spservice.getSpecialite().subscribe((res: any) => {
        this.listsp = res['data'];
      });
    }
  }