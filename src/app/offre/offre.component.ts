import { Component, OnInit } from '@angular/core';
import { OffreService } from '../offre.service';

@Component({
  selector: 'app-offre',
  templateUrl: './offre.component.html',
  styleUrls: ['./offre.component.css']
})
export class OffreComponent implements OnInit {
  listoffre:any;
  p:number=1;
  constructor(private offreService: OffreService) { }

  ngOnInit(): void {
    this.getOffre();
  }
getOffre(){
  return this.offreService.getallOffre().subscribe((res:any)=>{
    this.listoffre=res["data"]
    console.log('liste offre :', this.listoffre);
  });
  
}

confirmoffre(id: any, etat: any) {
return   this.offreService.confirmedOffrer(id,etat).subscribe((res: any) => {
    console.log(res);
    this.getOffre();
  });
}
}

