import { Component, OnInit } from '@angular/core';
import { EntrepriseService } from '../entreprise.service';

@Component({
  selector: 'app-entreprise',
  templateUrl: './entreprise.component.html',
  styleUrls: ['./entreprise.component.css']
})
export class EntrepriseComponent implements OnInit {
listentreprise:any;
p:number=1;
  constructor(private entrepriseService: EntrepriseService) { }

  ngOnInit(): void {
    this.getEntreprise();
  }
getEntreprise(){
  return this.entrepriseService.getallentreprises().subscribe((res:any)=>{
    this.listentreprise=res["data"].filter(
      (element: any) => element.role == 'entreprise'
    );
    console.log('liste entreprise :', this.listentreprise);
  });
  
}

confirmuser(id: any, etat: any) {
  this.entrepriseService.confirmeduser(id, etat).subscribe((res: any) => {
    console.log(res);
    this.getEntreprise();
  });
}
}
