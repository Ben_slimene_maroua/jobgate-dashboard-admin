import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ForgetPasswordService {
 
  
  constructor(private http: HttpClient) { }
  token = localStorage.getItem("token")!
  headersoption = new HttpHeaders({
    Authorization: 'Bearer ' + this.token
  })

forgetPassword(email:any){
return this.http.post(`${environment.baseUrl}/admin/forgetpassword`,email);
  }

  changePassword(newpwd:any){
    return this.http.put(`${environment.baseUrl}/admin/changepassword`,newpwd,{headers:this.headersoption});
  }
resetPassword(newpwd:any){
  return this.http.post(`${environment.baseUrl}/admin/resetpassword`,newpwd);
}
 

}
