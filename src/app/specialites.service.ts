import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SpecialitesService {

  constructor(private http: HttpClient) { }
  token=localStorage.getItem('token')!;
  headersoption= new HttpHeaders({
    Authorization: 'Bearer ' + this.token,
  });
  createSpecialite(sp:any){
    return this.http.post(`${environment.baseUrl}/specialite/createspecialite`,sp, {headers: this.headersoption})
  }

  getSpecialite(){
    return this.http.get(`${environment.baseUrl}/specialite/getspecialite`, {headers: this.headersoption});
  }; 
  deletSpecialite(id:any){
    return this.http.delete(`${environment.baseUrl}/specialite/deletespecialite/${id}`,{headers: this.headersoption});
  }
  updateSpecialite(id:any,sp:any){
    return this.http.put(`${environment.baseUrl}/specialite/upspecialite/${id}`,sp,{headers: this.headersoption});
  }
}