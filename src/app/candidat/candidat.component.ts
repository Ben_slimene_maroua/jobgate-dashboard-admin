import { Component, OnInit } from '@angular/core';
import { CandidatService } from '../candidat.service';

@Component({
  selector: 'app-candidat',
  templateUrl: './candidat.component.html',
  styleUrls: ['./candidat.component.css']
})
export class CandidatComponent implements OnInit {
  listcandidat:any;
  p:number=1;
  constructor(private candidatService:CandidatService ) { }

  ngOnInit(): void {
    this.getCandidats();
  }
getCandidats(){
  return this.candidatService.getallcandidats().subscribe((res:any)=>{
    this.listcandidat=res["data"].filter(
      (element: any) => element.role == 'candidat'
    );
    console.log('liste candidat:', this.listcandidat);
  });
  
}

confirmuser(id: any, etat: any) {
  this.candidatService.confirmeduser(id, etat).subscribe((res: any) => {
    console.log(res);
    this.getCandidats();
  });
}
}

