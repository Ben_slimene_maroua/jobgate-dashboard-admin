import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OffreService {

  constructor(private http: HttpClient) { }
  token=localStorage.getItem('token')!;
  headersoption= new HttpHeaders({
    Authorization: 'Bearer ' + this.token,
  });
  getallOffre(){
    return this.http.get(`${environment.baseUrl}/offre/getoffre`);
  }
  getoffre(id:any){
return this.http.get(`${environment.baseUrl}/offre/getoffreById/${id}`);
  }
  confirmedOffrer(id: any,etat:any){
    return this.http.put(`${environment.baseUrl}/admin/confirmoffre/${id}`,etat, {headers: this.headersoption})
  }
  deleteOffre(id:any){
    return this.http.delete(`${environment.baseUrl}/offre/deleteoffre/${id}`,{headers: this.headersoption})
  }
}